from IDeFIX.read_runinformation import get_bcl_list
from joblib import Parallel, delayed
from os.path import splitext
from IDeFIX import gzip
from IDeFIX.cbclreader import CBCLReader


# takes a byte value and returns a dna base
def convertbyte(byte):
    bcldict = ["A", "C", "G", "T"]
    if byte:
        return bcldict[byte & 3]
    return "N"


# reads filter file to define on which spots the bcl file has useful values
def readfilter(path):
    with open(path, "rb") as filter:
        header = filter.read(4)
        format = filter.read(4)
        cluster = filter.read(4)
        filterstring = filter.read()
        filterlist = [x for x in range(len(filterstring)) if filterstring[x]]
        return filterlist


def readfile(filename):
    if splitext(filename)[1] == ".cbcl":
        cbclreader = CBCLReader(filename)
        bytelist = cbclreader.read()
    else:
        try:
            with gzip.open(filename) as file:
                header = file.read(4)
                cluster = int.from_bytes(header, "little")
                bytestring = file.read()
                bytelist = [convertbyte(bytestring[x]) for x in range(len(bytestring))]
                # bytelist=[convertbyte(bytestring[x]) for x in filterlist]
        except OSError:
            with open(filename, "rb") as file:
                header = file.read(4)
                cluster = int.from_bytes(header, "little")
                bytestring = file.read()
                bytelist = [convertbyte(bytestring[x]) for x in range(len(bytestring))]
                # bytelist=[convertbyte(bytestring[x]) for x in filterlist]
    return bytelist


# collects the nucleobase lists of the different reads
def manage_parallel_bclreading(filesfwd, filesrev, singleend, threads):
    fwd = []
    rev = []
    for i in range(len(filesfwd)):
        fwd.append(Parallel(n_jobs=threads)(delayed(readfile)(j) for j in filesfwd[i]))
        if not singleend:
            rev.append(
                Parallel(n_jobs=threads)(delayed(readfile)(j) for j in filesrev[i])
            )
    return fwd, rev


def readlane(lanename, runfolder, runinfo, is_singleend, threads):
    bcl_files_fwd, bcl_files_rev = get_bcl_list(lanename, runfolder, runinfo)

    if is_singleend:
        fwdb, revb = manage_parallel_bclreading(
            bcl_files_fwd, [[]], is_singleend, threads
        )
    else:
        fwdb, revb = manage_parallel_bclreading(
            bcl_files_fwd, bcl_files_rev, is_singleend, threads
        )

    # merge nucleobase lists to index
    fwd = [
        ([item for sublist in x for item in sublist])
        for x in [list(x) for x in zip(*fwdb)]
    ]
    rev = [
        ([item for sublist in x for item in sublist])
        for x in [list(x) for x in zip(*revb)]
    ]

    fwdstr = ["".join(list(x)) for x in zip(*fwd)]
    revstr = ["".join(list(x)) for x in zip(*rev)]

    if is_singleend:
        return fwdstr

    final = ["-".join(list(x)) for x in zip(fwdstr, revstr)]
    return final
