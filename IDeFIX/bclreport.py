from os.path import join
from os.path import basename
from os.path import dirname
from IDeFIX import read_runinformation
from IDeFIX import reader
from IDeFIX import report


def check_ids_with_bcl(
    runfolder, runinfo, samplesheet, threshold, threads, fail_on_gap, is_singleend
):
    lanelist = read_runinformation.create_lanelist(runfolder)
    samples, samplesbylane, negatives, negativesperlane = (
        read_runinformation.get_samples(samplesheet, runinfo, is_singleend, "x")
    )
    all_indices = []
    for lane in lanelist:
        lanename = basename(lane)
        lanepath_for_lanename = dirname(lanename)
        while lanename == "":
            lanename = basename(lanepath_for_lanename)
            lanepath_for_lanename = dirname(lanename)
        print("NOW READING: LANE " + lanename)
        indexlist = []
        indexlist = reader.readlane(lane, runfolder, runinfo, is_singleend, threads)
        print("FINISHED READING: LANE " + lanename)
        all_indices.extend(indexlist)
        if len(lanelist) > 1:
            lane_indexreport, lane_missing_samples = report.analyze_result(
                indexlist, samples.copy(), negatives, threshold
            )
            report.dump_report(
                lane_indexreport, join(runfolder, f"IDeFIX_Report_{lanename}.tsv")
            )
            print(f"\n******* LANE-REPORT {lanename} *******\n")
            report.print_report(lane_missing_samples, lane_indexreport, False, True)

    print("\n******* REPORT *******\n")
    indexreport, missing_samples = report.analyze_result(
        all_indices, samples, negatives, threshold
    )
    report.dump_report(indexreport, join(runfolder, "IDeFIX_Report.tsv"))
    report.print_report(missing_samples, indexreport, fail_on_gap, False)
