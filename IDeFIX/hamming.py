from itertools import combinations
from pprint import pprint
from os.path import join
from sys import exit
from IDeFIX.Samplesheet import Samplesheet


def hamming(indices, mismatches):
    if mismatches == -1:
        minhamming = 5
    else:
        minhamming = 2 * mismatches + 1
    hammingerror = False
    hammingerrordic = {}
    for i in range(minhamming):
        hammingerrordic[i] = []
    for comb in combinations(indices, 2):
        try:
            hamming_distance = sum(
                el1 != el2 for el1, el2 in zip(comb[0][1], comb[1][1])
            )
            hammingerrordic[hamming_distance].append([comb[0][0], comb[1][0]])
            if hamming_distance != 0:
                hammingerror = True
        except KeyError:
            pass
    return hammingerrordic, hammingerror


def detmaxmismatch(hammingerrordic):
    for i in range(4):
        if len(hammingerrordic[i + 1]) != 0:
            if i + 1 < 3:
                return 0
            else:
                return 1
    return 2


def report(hammingerrordic, hammingerror, rundir, mismatches, index_str):
    if mismatches == -1:
        allowed_mismatches = detmaxmismatch(hammingerrordic)
        with open(join(rundir, "allowed_mismatch" + index_str + ".txt"), "w") as opfile:
            opfile.write(str(allowed_mismatches) + "\n")
        print(
            "\nAll indices are varying enough for "
            + str(allowed_mismatches)
            + " allowed mismatches in "
            + index_str
        )
        return allowed_mismatches
    if hammingerror:
        print(
            "ERROR! There are indices which are too close together to be distinguished with "
            + str(mismatches)
            + " allowed mismatches in "
            + index_str
        )
        print(
            "The following samples are to close together (with corresponding Hamming distance):"
        )
        pprint(hammingerrordic)
        file = open(join(rundir, "hamming_failed.txt"), "w")
        file.close()
        print("")
        exit(1)
    else:
        print(
            "All indices are varying enough for "
            + str(mismatches)
            + " allowed mismatches in "
            + index_str
            + "\n"
        )
        file = open(join(rundir, "hamming_completed.txt"), "w")
        file.close()
        return mismatches


def check_hamming_distance(
    samplesheet: Samplesheet, mismatches: int, rundir: str, singleend: bool
):
    allowed_mismatches_per_index = []
    try:
        indices_i1 = samplesheet.get_data()[["Sample_ID", "index"]].values.tolist()
    except KeyError:
        indices_i1 = samplesheet.get_data()[["Sample_ID", "Index"]].values.tolist()
    hammingerrordic_i1, hammingerror_i1 = hamming(indices_i1, mismatches)
    allowed_mismatches_per_index.append(
        report(hammingerrordic_i1, hammingerror_i1, rundir, mismatches, "index1")
    )

    if not singleend:
        try:
            indices_i2 = samplesheet.get_data()[["Sample_ID", "index2"]].values.tolist()
        except KeyError:
            indices_i2 = samplesheet.get_data()[["Sample_ID", "Index2"]].values.tolist()
        hammingerrordic_i2, hammingerror_i2 = hamming(indices_i2, mismatches)
        allowed_mismatches_per_index.append(
            report(hammingerrordic_i2, hammingerror_i2, rundir, mismatches, "index2")
        )

    minmismatches = min(allowed_mismatches_per_index)
    if mismatches == -1:
        with open(join(rundir, "allowed_mismatch.txt"), "w") as opfile:
            opfile.write(str(minmismatches) + "\n")
    print("")
    return minmismatches, allowed_mismatches_per_index
