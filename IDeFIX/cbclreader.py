import binascii
import io
from IDeFIX import gzip


class CBCLReader:
    def __init__(self, filepath):
        self.filepath = filepath
        (
            self.version,
            self.headersize,
            self.bitsperbasecall,
            self.bitsperqscore,
            self.bases,
        ) = self.readcbcl()

    def read(self):
        return self.bases

    def convertbyte(self, byte):
        bcldict = ["A", "C", "G", "T"]
        if byte:
            return bcldict[byte & 3]
        else:
            return "N"

    def gunzip_bytes_obj(self, bytes_obj, bitsperbasecall, bitsperqscore):
        in_ = io.BytesIO()
        in_.write(bytes_obj)
        in_.seek(0)
        with gzip.GzipFile(fileobj=in_, mode="rb") as fo:
            bases = ""
            pointer = 0
            fo.seek(0, 2)
            length = fo.tell()
            fo.seek(0)
            while pointer < length:
                base = fo.read(bitsperbasecall)
                qual = fo.read(bitsperqscore)
                pointer += bitsperbasecall + bitsperqscore
                bases += self.convertbyte(int.from_bytes(base, "little"))
        return bases

    def readcbcl(self):
        with open(self.filepath, "rb") as file:
            version = int.from_bytes(file.read(2), "little")
            headersize = int.from_bytes(file.read(4), "little")
            bitsperbasecall = int.from_bytes(file.read(1), "little")
            bitsperqscore = int.from_bytes(file.read(1), "little")
            file.read(headersize - 8)
            gz = file.read()
            assert binascii.hexlify(gz[0:2]) == b"1f8b"
            bcl = self.gunzip_bytes_obj(gz, bitsperbasecall, bitsperqscore)
            return version, headersize, bitsperbasecall, bitsperqscore, bcl
