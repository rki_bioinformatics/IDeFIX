from os.path import join
from os.path import basename
import xml.etree.ElementTree as ET
from glob import glob
import pandas as pd


def device_has_bcl_folders(runinfo):
    tree = ET.parse(runinfo)
    root = tree.getroot()
    for child in root.iter("Instrument"):
        if child.text.startswith("FS") or child.text.startswith("NB"):
            return False
    return True


def is_singleend(runinfo):
    tree = ET.parse(runinfo)
    root = tree.getroot()
    readamount = 0
    for read_element in root.iter("Reads"):
        for child in read_element:
            if child.attrib["IsIndexedRead"] == "Y":
                readamount += 1
    return readamount == 1


def create_lanelist(runfolder):
    lanelist = glob(
        join(runfolder, "Data", "Intensities", "BaseCalls", "L[0-9][0-9][0-9]")
    )
    lanelist.sort()
    if len(lanelist) < 1:
        raise ValueError(
            "ERROR! There are no lanes in "
            + join(runfolder, "Data", "Intensities", "BaseCalls")
        )
    return lanelist


def get_reverse_complement_reads(runinfo):
    result = []
    root = ET.parse(runinfo).getroot()
    for child in root:
        if child.tag == "Run":
            for grandchild in child:
                if grandchild.tag == "Reads":
                    for read in grandchild:
                        if read.attrib["IsIndexedRead"] == "Y":
                            try:
                                result.append(read.attrib["IsReverseComplement"] == "Y")
                            except KeyError:
                                result.append(False)
    return result


def reversecom(index):
    out = ""
    transl = {"A": "T", "T": "A", "C": "G", "G": "C", "N": "N"}
    for i in index:
        out = transl[i] + out
    return out


def get_samples(samplesheet, runinfo, singleend, negativecode):
    samplesbylane = {}
    negativesperlane = {}
    reverse_complement_reads = get_reverse_complement_reads(runinfo)
    sample_df = samplesheet.get_data()
    sample_df.columns = map(str.lower, sample_df.columns)

    for index, is_reverse_complement in enumerate(reverse_complement_reads):
        if is_reverse_complement:
            if index == 0:
                sample_df["index"] = sample_df["index"].apply(reversecom)
            if index == 1:
                sample_df["index2"] = sample_df["index2"].apply(reversecom)

    if singleend:
        try:
            sample_df["whole_index"] = sample_df["index"]
        except KeyError:
            sample_df["whole_index"] = sample_df["Index"]
    else:
        try:
            sample_df["whole_index"] = sample_df["index"] + "-" + sample_df["index2"]
        except KeyError:
            sample_df["whole_index"] = sample_df["Index"] + "-" + sample_df["Index2"]
    try:
        negative_samples = sample_df[sample_df["description"] == negativecode]
    except KeyError:
        negative_samples = pd.DataFrame(columns=sample_df.columns)

    samples = {
        row["sample_id"]: row["whole_index"] for index, row in sample_df.iterrows()
    }
    negatives = {
        row["sample_id"]: row["whole_index"]
        for index, row in negative_samples.iterrows()
    }

    if "lane" in sample_df.columns:
        samplesbylane = {}
        negativesperlane = {}
        lanes = sample_df["lane"].unique()
        for lane in lanes:
            lanename = f"L{int(lane):03d}"
            lane_samples = {
                row["sample_id"]: row["whole_index"]
                for index, row in sample_df.iterrows()
                if row["lane"] == lane
            }
            samplesbylane[lanename] = lane_samples
            lane_samples_negative = {
                row["sample_id"]: row["whole_index"]
                for index, row in negative_samples.iterrows()
                if row["lane"] == lane
            }
            negativesperlane[lanename] = lane_samples_negative
    else:
        samplesbylane = {}
        negativesperlane = {}
    return samples, samplesbylane, negatives, negativesperlane


def _get_bcl_for_folder_runs(runfolder, lanename, runinfo_root):
    reads = 1
    index = 0
    filetypes = ["*.bcl", "*.bcl.*", "*.cbcl"]
    cyclesfwd = []
    cyclesrev = []
    for filetype in filetypes:
        files = [
            basename(x)
            for x in (
                glob(
                    join(
                        runfolder,
                        "Data",
                        "Intensities",
                        "BaseCalls",
                        lanename,
                        "C1.1",
                        filetype,
                    )
                )
            )
        ]
        if not len(files) == 0:
            break
    if len(files) == 0:
        raise Exception(
            "ERROR!! No relevant bcl-files can be found in "
            + join(runfolder, "Data", "Intensities", "BaseCalls", lanename)
        )
    for child in runinfo_root.iter("Read"):
        if child.get("IsIndexedRead") == "Y":
            if index == 0:
                cyclesfwd = [
                    join(
                        runfolder,
                        "Data",
                        "Intensities",
                        "BaseCalls",
                        lanename,
                        "C{}.1".format(str(i)),
                    )
                    for i in range(reads, reads + int(child.get("NumCycles")))
                ]
                index += 1
            else:
                cyclesrev = [
                    join(
                        runfolder,
                        "Data",
                        "Intensities",
                        "BaseCalls",
                        lanename,
                        "C{}.1".format(str(i)),
                    )
                    for i in range(reads, reads + int(child.get("NumCycles")))
                ]
                index += 1
        reads += int(child.get("NumCycles"))

    filesfwd = [[join(x, tile) for x in cyclesfwd] for tile in files]
    filesrev = [[join(x, tile) for x in cyclesrev] for tile in files]
    return filesfwd, filesrev


def _get_bcl_for_file_runs(runfolder, lanename, runinfo_root):
    reads = 0
    index = 0
    filetypes = ["*.bcl.bgzf", "*.bcl"]
    filesfwd = []
    filesrev = []
    for filetype in filetypes:
        bcl_files = glob(
            join(runfolder, "Data", "Intensities", "BaseCalls", lanename, filetype)
        )
        if not len(bcl_files) == 0:
            break
    bcl_files.sort()
    for child in runinfo_root.iter("Read"):
        if child.get("IsIndexedRead") == "Y":
            if index == 0:
                filesfwd = [bcl_files[reads : reads + int(child.get("NumCycles"))]]
                index += 1
            else:
                filesrev = [bcl_files[reads : reads + int(child.get("NumCycles"))]]
                index += 1
        reads += int(child.get("NumCycles"))

    return filesfwd, filesrev


def get_bcl_list(lanename, runfolder, runinfo):
    tree = ET.parse(runinfo)
    root = tree.getroot()
    singleend = is_singleend(runinfo)
    # gets index relevant files for MiSeq/HiSeq runs
    if device_has_bcl_folders(runinfo):
        filesfwd, filesrev = _get_bcl_for_folder_runs(runfolder, lanename, root)
    # gets index relevant files for NextSeq/iSeq runs
    else:
        filesfwd, filesrev = _get_bcl_for_file_runs(runfolder, lanename, root)

    # check if files could be found
    if len(filesfwd[0]) == 0:
        raise OSError(
            "ERROR!! No relevant bcl-files can be found in "
            + join(runfolder, "Data", "Intensities", "BaseCalls", lanename)
        )
    if not singleend:
        if len(filesrev[0]) == 0:
            raise OSError(
                "ERROR!! No relevant bcl-files can be found in "
                + join(runfolder, "Data", "Intensities", "BaseCalls", lanename)
            )
    return filesfwd, filesrev
