import pandas as pd
import re


class Samplesheet:
    def __init__(self, path, separator=",") -> None:
        self.path = path
        self.separator = separator
        self.section_content = self._read_samplesheet()

    def _read_samplesheet(self):
        section_content = {}
        sections_titles = [
            "[Header]",
            "[Reads]",
            "[Settings]",
            "[BCLConvert_Settings]",
            "[Data]",
            "[BCLConvert_Data]",
        ]
        current_lines = []
        current_section = ""

        with open(self.path, "r", encoding="utf8") as samplesheet:
            for line in samplesheet:
                splitted_line = line.strip().split(self.separator)
                if splitted_line[0] in sections_titles:
                    if len(current_lines) != 0:
                        section_content[current_section] = current_lines.copy()
                    current_lines.clear()
                    current_section = splitted_line[0]
                else:
                    current_lines.append(line.strip())
        section_content[current_section] = current_lines.copy()
        return section_content

    def add_to_settings(self, new_key, new_val, fail_on_existence=False):
        new_settings = []
        key_found = False
        if "[Settings]" in self.section_content.keys():
            section_key = "[Settings]"
        else:
            section_key = "[BCLConvert_Settings]"
        for line in self.section_content[section_key]:
            line_split = line.split(self.separator)
            if line_split[0].strip() == new_key:
                if fail_on_existence:
                    raise ValueError("Key already exists")
                line_split[1] = str(new_val)
                key_found = True
            new_settings.append(self.separator.join(line_split))
        if not key_found:
            new_settings.insert(0, self.separator.join([str(new_key), str(new_val)]))
        self.section_content[section_key] = new_settings

    def delete_adapter(self):
        adapter_possibilities = ["Adapter", "AdapterRead1", "AdapterRead1"]
        new_settings = []
        if "[Settings]" in self.section_content.keys():
            section_key = "[Settings]"
        else:
            section_key = "[BCLConvert_Settings]"
        for line in self.section_content[section_key]:
            line_split = line.split(self.separator)
            if line_split[0].strip() not in adapter_possibilities:
                new_settings.append(self.separator.join(line_split))
        self.section_content[section_key] = new_settings

    def lower_users(self):
        data = self.get_data()
        data["Sample_Project"] = data["Sample_Project"].str.lower()
        if "[Data]" in self.section_content.keys():
            section_key = "[Data]"
        else:
            section_key = "[BCLConvert_Data]"
        self.section_content[section_key] = [
            self.separator.join(v)
            for v in [data.columns.tolist()] + data.values.tolist()
        ]

    def _correct_column_chars_regex(self, string):
        return re.sub("[^a-zA-Z0-9_\n-]", "", string)

    def correct_column_chars(self, columnname):
        data = self.get_data()
        data[columnname] = data[columnname].apply(self._correct_column_chars_regex)
        if "[Data]" in self.section_content.keys():
            section_key = "[Data]"
        else:
            section_key = "[BCLConvert_Data]"
        self.section_content[section_key] = [
            self.separator.join(v)
            for v in [data.columns.tolist()] + data.values.tolist()
        ]

    def dump_samplesheet(self, path):
        sections_titles = [
            "[Header]",
            "[Reads]",
            "[Settings]",
            "[BCLConvert_Settings]",
            "[Data]",
            "[BCLConvert_Data]",
        ]
        with open(path, "w") as corrected_samplesheet:
            for section in sections_titles:
                if section in self.section_content.keys():
                    corrected_samplesheet.write(section + "\n")
                    for content in self.section_content[section]:
                        corrected_samplesheet.write(content + "\n")

    def _lines_to_dic(self, lines):
        dic = {}
        for line in lines:
            splitted_line = line.split(self.separator)
            if len(splitted_line) <= 0:
                continue
            elif len(splitted_line) == 1:
                dic[splitted_line[0]] = ""
            else:
                dic[splitted_line[0]] = splitted_line[1]
        return dic

    def get_header(self):
        try:
            lines = self.section_content["[Header]"]
        except KeyError:
            return None
        return self._lines_to_dic(lines)

    def get_reads(self):
        try:
            lines = self.section_content["[Reads]"]
        except KeyError:
            return None
        return [
            line.split(self.separator)[0]
            for line in lines
            if line.split(self.separator)[0] != ""
        ]

    def get_settings(self):
        try:
            lines = self.section_content["[Settings]"]
        except KeyError:
            try:
                lines = self.section_content["[BCLConvert_Settings]"]
            except KeyError:
                return None
        return self._lines_to_dic(lines)

    def add_data_column(self, title, case_insensitive_check):
        if "[Data]" in self.section_content.keys():
            section_key = "[Data]"
        else:
            section_key = "[BCLConvert_Data]"
        header = self.section_content[section_key][0].split(self.separator)
        if case_insensitive_check:
            if title.upper() in map(str.upper, header):
                return False
        else:
            if title in header:
                return False
        header.append(title)
        self.section_content[section_key][0] = self.separator.join(header)
        for i in range(1, len(self.section_content[section_key])):
            row = self.section_content[section_key][i].split(self.separator)
            row.append("")
            self.section_content[section_key][i] = self.separator.join(row)
        return True

    def remove_data_column(self, title, case_insensitive_check):
        if "[Data]" in self.section_content.keys():
            section_key = "[Data]"
        else:
            section_key = "[BCLConvert_Data]"
        header = self.section_content[section_key][0].split(self.separator)
        if case_insensitive_check:
            try:
                index = list(map(str.upper, header)).index(title.upper())
            except ValueError:
                return False
        else:
            try:
                index = header.index(title)
            except ValueError:
                return False

        header.pop(index)
        self.section_content[section_key][0] = self.separator.join(header)
        for i in range(1, len(self.section_content[section_key])):
            row = self.section_content[section_key][i].split(self.separator)
            row.pop(index)
            self.section_content[section_key][i] = self.separator.join(row)
        return True

    def get_data(self):
        try:
            lines = self.section_content["[Data]"]
            if len(lines) <= 0:
                return None
        except KeyError:
            try:
                lines = self.section_content["[BCLConvert_Data]"]
            except KeyError:
                return None
        splitted_lines = [line.strip().split(self.separator) for line in lines]
        return pd.DataFrame(splitted_lines[1:], columns=splitted_lines[0])

    def set_data(self, new_data):
        if "[Data]" in self.section_content.keys():
            section_key = "[Data]"
        else:
            section_key = "[BCLConvert_Data]"
        self.section_content[section_key] = [self.section_content[section_key][0]] + [
            self.separator.join(i) for i in new_data
        ]
