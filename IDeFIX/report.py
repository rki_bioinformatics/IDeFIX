from collections import Counter
from pprint import pprint
import sys


def analyze_result(indexlist, samples, negatives, threshold):
    indicesfound = Counter(indexlist).most_common()
    threshold_filtered_indices = [
        index for index in indicesfound if index[1] > threshold
    ]
    matched_indices = []
    check = True
    for index in threshold_filtered_indices:
        if check:
            matching_samples = [k for k, v in samples.items() if v == index[0]]
            if len(matching_samples) >= 1:
                matched_indices.append([index[0], index[1], matching_samples])
                for sample in matching_samples:
                    del samples[sample]
                if len(samples) == 0:
                    check = False
            elif len(matching_samples) == 0:
                matched_indices.append([index[0], index[1], []])
        else:
            matched_indices.append([index[0], index[1], []])
    for sample in samples.copy().keys():
        if sample in negatives.keys():
            del samples[sample]
    return matched_indices, samples


def dump_report(indices, output):
    for index in indices:
        index[1] = str(index[1])
        index[2] = ",".join(index[2])
    formatted_indices = ["\t".join(line) + "\n" for line in indices]
    with open(output, "w") as op:
        op.write("INDEX\tAMOUNT\tSAMPLES\n")
        op.writelines(formatted_indices)


def _has_indexgap(indices, fail_on_relevant_gap, lanereport):
    empty = False
    empty_with_relevant_index = False
    has_indexgap = False

    for index in indices:
        if index[2].strip() == "":
            empty = True
            if len(set(index[0])) > 1:
                empty_with_relevant_index = True
        else:
            if empty:
                if empty_with_relevant_index and fail_on_relevant_gap and not lanereport:
                    raise ValueError(
                        "There is a gap in the actual abundance of the indices in the Sample Sheet.\n"
                        "Meaning, there are either indices present in great quantity, which are not in the Sample Sheet,\n"
                        "or there are indices in the Sample Sheet which are present in low abundance. \n"
                        "For detailed information, please see IDeFIX_Report.csv.\n"
                    )
                has_indexgap = True
    return has_indexgap


def print_report(missing_samples, indices, fail_on_relevant_gap, lanereport):
    fail = False
    if len(missing_samples) == 0:
        print(
            "Every index(-combination) in the Sample Sheet is present in the raw data as well."
        )
    else:
        print(
            "ERROR! There are indices in the Sample Sheet, which are missing in the raw data:"
        )
        pprint(missing_samples)
        fail = True

    print("")

    if _has_indexgap(indices, fail_on_relevant_gap, lanereport):
        print(
            "There is a gap in the actual abundance of the indices in the Sample Sheet.\n"
            "Meaning, there are either indices present in great quantity, which are not in the Sample Sheet,\n"
            "or there are indices in the Sample Sheet which are present in low abundance. \n"
            "For detailed information, please see IDeFIX_Report.csv.\n"
        )
    else:
        print(
            "All indices in the Sample Sheet are those with the highest actual abundance in the raw data.\nMeaning, "
            "there are neither indices present in great quantity, which are not in the Sample Sheet,\nnor are there "
            "indices in the Sample Sheet which are present in low abundance.\n"
            "For detailed information, please see IDeFIX_Report.csv.\n"
        )
    if fail and not lanereport:
        sys.exit(1)
