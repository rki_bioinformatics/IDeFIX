#!/usr/bin/env python3

import argparse
import os
import sys
from pathlib import Path
from IDeFIX import bclreport
from IDeFIX import Samplesheet
from IDeFIX import hamming
from IDeFIX import read_runinformation


def parse():
    parser = argparse.ArgumentParser(
        prog="IDeFIX",
        description='Index abundance report from Illumina NGS raw data. \n To get the current version, type "IDeFIX.py version"'
        "and Sample Sheet correction",
    )
    parser.add_argument(
        "-p", "--projectpath", help="The path of the (top-level) project folder."
    )
    parser.add_argument(
        "-v", "--version", help="Shows IDeFIX version and quits.", action="store_true"
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=int,
        default=1000,
        help="Threshold for minimum number of reads of an index to be reported. "
        "(default: 1000)",
    )
    parser.add_argument(
        "-s",
        "--samplesheet_name",
        default="SampleSheet.csv",
        help="Defines the name of the Sample Sheet.",
    )
    parser.add_argument(
        "-S",
        "--samplesheet_output",
        default="CorrectedSampleSheet.csv",
        help="Defines the name of the corrected SampleSheet which will be created.",
    )
    parser.add_argument(
        "-d",
        "--only_bcl_check",
        action="store_true",
        help="Don't remove undesired characters on the SampleSheet, just check for the amount of existing indices in bcl files.",
    )
    parser.add_argument(
        "-C",
        "--only_char_correct",
        action="store_true",
        help="Removal of undesired characters (backslashes, dots, tabs and whitespaces as well as "
        "umlauts) only, no bcl check will be performed.",
    )
    parser.add_argument(
        "-a",
        "--deleteadapter",
        action="store_true",
        help="Deletes the adapter sequence from samplesheet.",
    )
    parser.add_argument(
        "-m",
        "--mismatches",
        default=1,
        type=int,
        help="Amount of allowed mismatches in bcl2fastq/bclconvert to check indices. If -1, check for highest possible mismatch number and write it to projectpath/allowed_mismatch.txt",
    )
    parser.add_argument(
        "-H",
        "--dont_check_hamming",
        action="store_true",
        help="Don't check the indices for Hamming distance.",
    )
    parser.add_argument(
        "-f",
        "--fail_on_gap",
        action="store_true",
        help="Fail, if there there are indices without sample with are more present in the run than indices with sample.",
    )
    parser.add_argument(
        "-A",
        "--add_mismatches_to_samplesheet",
        help="Add either the specified or the highest possible mismatch number (depending on -m option) to the Settings section of the Samplesheet. "
        + 'Options: " -A MINMM ": Use lowest allowed mismatch to have mismatches for both indices uniformly; " -A MMPERINDEX ": Use mismatch specifically calculated for regarding index ',
        required=False,
    )
    parser.add_argument(
        "-T",
        "--threads",
        default=20,
        type=int,
        help="Amount of threads to use in BCL reading.",
    )

    return parser


def get_run_info(runpath):
    """
    get name of run info file, if the file is missing, exit with error
    """
    files = os.listdir(runpath)
    for i in files:
        if i.lower() == "runinfo.xml":
            return i
            break
    print("ERROR: There is no RunInfo in " + runpath)
    sys.exit(1)


def exists(path):
    """
    file existance test
    """
    if os.path.isfile(path):
        return
    else:
        print("ERROR: The file " + path + " does not exist.")
        sys.exit(1)


def main():
    parser = parse()
    args = parser.parse_args()
    if args.version:
        try:
            with open(
                os.path.join(os.path.dirname(os.path.abspath(__file__)), "version.txt"),
                "r",
            ) as x:
                print("IDeFIX " + x.read().strip())
        except FileNotFoundError:
            print(
                "ERROR! Versions-file could not be found. Please download and unzip IDeFIX again."
            )
            sys.exit(1)
        sys.exit(0)

    print(
        """
  _____   _____           ______   _____  __   __
 |_   _| |  __ \         |  ____| |_   _| \ \ / /
   | |   | |  | |   ___  | |__      | |    \ V / 
   | |   | |  | |  / _ \ |  __|     | |     > <  
  _| |_  | |__| | |  __/ | |       _| |_   / . \ 
 |_____| |_____/   \___| |_|      |_____| /_/ \_\
          """
    )

    if args.projectpath is None:
        print("ERROR! A project path must be given with -p or --projectpath argument")
        sys.exit(1)
    print("\nParsed arguments: ", str(args))
    runpath = str(Path(args.projectpath).resolve())

    samplesheetname = os.path.join(runpath, args.samplesheet_name)
    corrected_samplesheet = os.path.join(runpath, args.samplesheet_output)
    runinfo = os.path.join(runpath, get_run_info(runpath))
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    singleend = read_runinformation.is_singleend(runinfo)
    exists(samplesheetname)

    samplesheet = Samplesheet.Samplesheet(samplesheetname)
    if args.deleteadapter:
        samplesheet.delete_adapter()
    if not args.only_bcl_check:
        samplesheet.lower_users()
        samplesheet.correct_column_chars("Sample_ID")
        try:
            samplesheet.correct_column_chars("Sample_Name")
        except KeyError:
            pass
        samplesheet.correct_column_chars("Sample_Project")
    try:
        if not args.dont_check_hamming:
            minmismatches, mismatchperindex = hamming.check_hamming_distance(
                samplesheet, args.mismatches, runpath, singleend
            )
            if args.add_mismatches_to_samplesheet != None:
                if args.add_mismatches_to_samplesheet == "MINMM":
                    samplesheet.add_to_settings(
                        "BarcodeMismatchesIndex1", minmismatches
                    )
                    if not singleend:
                        samplesheet.add_to_settings(
                            "BarcodeMismatchesIndex2", minmismatches
                        )
                elif args.add_mismatches_to_samplesheet == "MMPERINDEX":
                    samplesheet.add_to_settings(
                        "BarcodeMismatchesIndex1", mismatchperindex[0]
                    )
                    if not singleend:
                        samplesheet.add_to_settings(
                            "BarcodeMismatchesIndex2", mismatchperindex[1]
                        )
                else:
                    raise ValueError(
                        "Only MINMM and MMPERINDEX are allowed as options for -A/--add_mismatches_to_samplesheet, see --help for further explanations."
                    )
    finally:
        samplesheet.dump_samplesheet(corrected_samplesheet)
    if not args.only_char_correct:
        bclreport.check_ids_with_bcl(
            runpath,
            runinfo,
            samplesheet,
            args.threshold,
            args.threads,
            args.fail_on_gap,
            singleend,
        )


if __name__ == "__main__":
    main()
